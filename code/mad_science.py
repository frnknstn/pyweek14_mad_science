#!/usr/bin/python
"""PyWeek 14 'Mad Science' Entry

"""

from __future__ import division



import os
import sys
import time
import string

import pygame


from oyoyo.client import IRCClient
from oyoyo.cmdhandler import DefaultCommandHandler
from oyoyo import helpers


#import


class IRCNickname(str):
    """
    RFC 1459: Because of IRC's scandanavian origin, the characters {}| are
    considered to be the lower case equivalents of the characters []\,
    respectively. This is a critical issue when determining the equivalence of
    two nicknames.
    
    """

    irc_to_upper = string.maketrans("{}|", "[]\\")
    irc_to_lower = string.maketrans("[]\\", "{}|")
    
    def capitalize(self):
        retval = str.capitalize(self)

        if len(self) > 1:
            retval = retval[0].translate(self.irc_to_upper) + retval[1:]
        elif len(self) == 1:
            retval = retval.translate(self.irc_to_upper)

        return retval
        
    def upper(self):
        return str.upper(self).translate(self.irc_to_upper)
        
    def lower(self):
        return str.lower(self).translate(self.irc_to_lower)


class MyHandler(DefaultCommandHandler):
    # Handle messages (the PRIVMSG command, note lower case)
    def privmsg(self, nick, chan, msg):
        print "%s in %s said: %s" % (nick, chan, msg)

    def __unhandled__(self, *args):
        print("Unhandled:", args)

    def motd(self, server, nick, message):
        global motd
        motd = "\n".join((motd, message))
        




def main():
    # pygame.init()
    print("Main")

def connect(nick="CokeNotPepsi"):
    global cli
    global conn
    
    cli = IRCClient(MyHandler, host="localhost", port=6667, nick=nick)
    conn = cli.connect()


def q():
    helpers.quit(cli, msg="bye!")
    print cli, conn


def loop():
    try:
        while True:
            conn.next()
            time.sleep(0.2)
            sys.stdout.write(".")
            pass
        
    except KeyboardInterrupt:
        print("Stopping poll loop")
        
        


cli = None
conn = None

motd = ""

if __name__ == "__main__":
    main()
