#!/usr/bin/python -u
"""Field Commander Igor - game for PyWeek 14

"""
from __future__ import division, print_function

import os
import sys
import time
import itertools
import json

import pygame

from defines import * 
from region import Region


def load_regions(directory):
    """Load the region data files from the supplied directory.
    Returns a list of regions.

    """
    pass


def main():
    pygame.init()
    screen = pygame.display.set_mode(SCREEN_SIZE)

    regions = load_regions(REGION_DIR)

    debug("Entering main loop")
    running = True

    while running:
        time.sleep(0.001)
        pygame.time.wait(10)

        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key in (pygame.K_ESCAPE, pygame.K_q):
                    running = False
                else:
                    debug(event)

            if event.type == pygame.QUIT:
                running = False

            if event.type == pygame.MOUSEBUTTONDOWN:
                # clicked on a region!

                selected_region = None
                for region in regions.values():
                    if region.collide_point(event.pos):
                        selected_region = region
                        break

                if selected_region is None:
                    # no region selected
                    continue

                debug("You clicked %s region %d! Neighbors: %s" % (
                    str(event.pos),
                    selected_region.identifier,
                    str([ x.identifier for x in selected_region.neighbors]))
                      )

    debug("Main loop ended")
    screen.fill((0,0,0))
    pygame.display.flip()


if __name__ == "__main__":
    try:
        main()
    finally:
        pygame.quit()
    debug("Program ending...")
