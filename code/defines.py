#!/usr/bin/python -u
from __future__ import division, print_function

DEBUG = True

SCREEN_SIZE = (1024, 768)

REGION_DIR = "regions"
REGION_FILE = "regions.dat"


def debug(*args):
    if DEBUG:
        print(*args)


def color_to_hex(color):
    """convert a 3-tuple color to a hex string representing that color, with leading '0x'"""
    return "0x%02X%02X%02X" % color


if __name__ == "__main__":
    import game
    try:
        game.main()
    finally:
        game.pygame.quit()
    print("Program ending...")
