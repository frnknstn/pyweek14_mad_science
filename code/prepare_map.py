#!/usr/bin/python
"""Convert map_regions.png into individial region masks and a regions.dat file

"""

from __future__ import division, print_function

import os
import sys
import time
import itertools
import json

import pygame
import PIL.Image
import PIL.ImageChops

from defines import * 
from region import Region

SCREEN_SIZE = (800, 768)

def split_regions(img):
    """split an image into regions of a single colour"""

    colors = img.getcolors()

    # ignore white and black regions
    colors = [ x for x in colors if x[1] != (0, 0, 0) and x[1] != (255, 255, 255) ]

    debug("Map has %d regions" % (len(colors)))

    regions = {}

    for count, data in enumerate(colors):
        pixel_count, color = data

        filename = "region_%d_%s.png" % (count, color_to_hex(color))
        filename = os.path.join(REGION_DIR, filename)
        debug("Color %s has %d pixels. Saving mask to %s" % (str(color), pixel_count, filename))

        # crop down to the minimum region size, but remember our position
        mask = make_mask(img, color)
        bbox = mask.getbbox()
        mask = mask.crop(bbox)

        mask.save(open(filename, "wb"))

        # create the Region object
        x1, y1, x2, y2 = bbox
        rect = pygame.Rect((x1, y1), (x2 - x1, y2 - y1))
        mask_surf = pygame.image.fromstring(mask.convert("RGB").tostring(),
                                            rect.size,
                                            "RGB")

        regions[color] = Region(count, rect, mask_surf)

    return regions


def make_mask(img, color):
    """Create a black and white mask image from all the pixels of a given color"""
    background = PIL.Image.new(img.mode, img.size, color)
    delta = PIL.ImageChops.difference(img, background)

    # convert the delta to a monochrome mask
    transform = ([255] + [0] * 255)
    mask = delta.convert("L").point(transform)

    return mask


def find_neighbors(img, regions):
    """Process an image to determine which regions are adjacent."""
    if not isinstance(regions, dict):
        raise TypeError("Expected dict of regions")

    debug("")
    debug("Finding neighboring regions...")

    # let's do this the smarter way:
    # for each region, try colide with all the other regions (within 15 pixels or so)

    max_distance = 15

    for color, region in regions.items():
        outline = region.mask.outline(4)

        # first test for proximity
        proximity = region.rect.inflate(2 * max_distance, 2 * max_distance)
        candidates = [ x for x in regions.values() if x != region ]
        candidates = [ candidates[x] for x in proximity.collidelistall(candidates) ]


        debug("region %d has %d points in its outline, and %d nearby regions." %
              (region.identifier, len(outline), len(candidates)))

        
        for point in outline:
            found_right = False
            found_below = False
            
            for i in range(1, max_distance):
                right_pos = (region.rect.left + point[0] + i, region.rect.top + point[1])
                below_pos = (region.rect.left + point[0], region.rect.top + point[1] + i)
                
                for target in candidates:
                    if not found_right and target.collide_point(right_pos):
                        if target not in region.neighbors:
                            debug("   -> Found new neighbour %d to the right, at %s range %d" % (
                                target.identifier, str(right_pos), i))
                                  
                        region.add_neighbor(target)
                        found_right = True

                    if not found_below and target.collide_point(below_pos):
                        if target not in region.neighbors:
                            debug("   V  Found new neighbour %d to below, at %s range %d" % (
                                target.identifier, str(below_pos), i))

                        region.add_neighbor(target)
                        found_below = True

                if found_right and found_below:
                    # only take one region per pixel / direction
                    break

    # show our results
    debug("")
    debug("Found neighbors:")
    for color, region in regions.items():
        neighbors = ", ".join([ str(x.identifier) for x in region.neighbors ])        
        debug("Region %d - %s: %s" % (region.identifier, color_to_hex(color), neighbors))


def save_region_data(regions):
    """Write the supplied dict of regions to the regions data file in JSON format"""
    debug("")
    debug("Saving region data file...")
    data = []
    for color, region in regions.items():
        data.append(region)

    json.dump(data, open(os.path.join(REGION_DIR, REGION_FILE), "w"),
              sort_keys=True, indent=4, default=Region.to_json)


def remove_old_data():
    """Remove data that was probably created by previous runs of this tool"""

    debug("")
    debug("Removing old region data...")

    filepath = os.path.join(REGION_DIR, "regions.dat")
    if os.path.isfile(filepath):
        os.unlink(filepath)

    for filename in os.listdir(REGION_DIR):
        filepath = os.path.join(REGION_DIR, filename)
        if os.path.isfile(filepath) and \
           filename.lower().endswith(".png") and \
           filename.lower().startswith("region_") and \
           "_0x" in filename:
            os.unlink(filepath)


def init():
    debug("Creating data directories")

    for dirname in (REGION_DIR,):
        if not os.path.isdir(dirname):
            debug("Making directort %s" % dirname)
            os.mkdir(dirname)


def init_display():
    pygame.init()
    screen = pygame.display.set_mode(SCREEN_SIZE)

    return screen


def main():
    init()
    screen = init_display()

    # show map regions
    region_surf = pygame.image.load(os.path.join(REGION_DIR, "map_regions.png"))
    surf_string = pygame.image.tostring(region_surf, "RGB")

    region_img = PIL.Image.fromstring("RGB", region_surf.get_size(), surf_string)

    screen.blit(region_surf, (0,0,0,0))
    pygame.display.flip()

    remove_old_data()

    regions = split_regions(region_img)

    # draw the numbers
    numbers = []
    font = pygame.font.SysFont(None, 18)
    for region in regions.values():
        text = str(region.identifier)
        surf = font.render(text, True, (0,0,0))
        numbers.append((surf, region.centroid))
        screen.blit(surf, region.centroid)
    pygame.display.flip()
    
    find_neighbors(region_img, regions)

    # draw the adjacency
    for region in regions.values():
        start = region.centroid
        for neighbor in region.neighbors:
            end = neighbor.centroid
            pygame.draw.aaline(screen, (200,200,200), start, end)
    [ screen.blit(x, y) for x, y in numbers ]
    pygame.display.flip()    

    debug("")
    save_region_data(regions)


    # main loop
    debug("")
    debug("Entering main loop")
    running = True

    while running:
        time.sleep(0.001)
        pygame.time.wait(10)

        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key in (pygame.K_ESCAPE, pygame.K_q):
                    running = False
                else:
                    debug(event)


            if event.type == pygame.QUIT:
                running = False

            if event.type == pygame.MOUSEBUTTONDOWN:
                # clicked on a region!

                selected_region = None
                for region in regions.values():
                    if region.collide_point(event.pos):
                        selected_region = region
                        break

                if selected_region is None:
                    # no region selected
                    continue

                debug("You clicked %s region %d! Neighbors: %s" % (
                    str(event.pos),
                    selected_region.identifier,
                    str([ x.identifier for x in selected_region.neighbors]))
                      )


    debug("Main loop ended")
    screen.fill((0,0,0))
    pygame.display.flip()


if __name__ == "__main__":
    try:
        main()
    finally:
        pygame.quit()
    debug("Program ending...")

