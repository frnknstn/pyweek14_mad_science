import pygame

def test_threshold(fill_color, mask_color, threshold):
    surf = pygame.Surface((20, 20))
    surf.fill(fill_color)
    mask = pygame.mask.from_threshold(surf, mask_color, threshold)
    print("Fill of %s, from_threshold finds %d pixels of color %s when using a threshhold %s" %
          (str(fill_color), mask.count(), str(mask_color), str(threshold)))



def main():
    pygame.init()
    pygame.display.set_mode((320, 240))

    print("Select an exact color:")

    test_threshold(fill_color=(127,127,127),
                   mask_color=(127,127,127),
                   threshold=(0,0,0))

    test_threshold(fill_color=(127,127,127),
                   mask_color=(127,127,127),
                   threshold=(1,1,1))

    print("")
    print("Are we able to select ALL pixels in range?")
    test_threshold(fill_color=(255,255,255),
                   mask_color=(0,0,0),
                   threshold=(255,255,255))

    test_threshold(fill_color=(0,0,0),
                   mask_color=(255,255,255),
                   threshold=(255,255,255))

    print("")
    print("Inspecting the behaviour for a variety of colors:")


    test_threshold(fill_color=(0,0,0),
                   mask_color=(127,127,127),
                   threshold=(100,100,100))

    test_threshold(fill_color=(20,20,20),
                   mask_color=(127,127,127),
                   threshold=(100,100,100))

    test_threshold(fill_color=(27,27,27),
                   mask_color=(127,127,127),
                   threshold=(100,100,100))
    
    test_threshold(fill_color=(28,28,28),
                   mask_color=(127,127,127),
                   threshold=(100,100,100))
    
    test_threshold(fill_color=(100,100,100),
                   mask_color=(127,127,127),
                   threshold=(100,100,100))
    
    test_threshold(fill_color=(127,127,127),
                   mask_color=(127,127,127),
                   threshold=(100,100,100))

    test_threshold(fill_color=(226,226,226),
                   mask_color=(127,127,127),
                   threshold=(100,100,100))

    test_threshold(fill_color=(227,227,227),
                   mask_color=(127,127,127),
                   threshold=(100,100,100))

    test_threshold(fill_color=(255,255,255),
                   mask_color=(127,127,127),
                   threshold=(100,100,100))



if __name__ == "__main__":
    try:
        main()
    finally:
        pygame.quit()

