#!/usr/bin/python -u

import pygame

from defines import *

class Region(object):
    """region on the map"""

    def __init__(self, identifier, rect = None, mask_surf = None):
        self.neighbors = set()
        self.identifier = identifier

        if rect is None:
            self.rect = None
        else:
            self.rect = pygame.Rect(rect)

        self.set_mask(mask_surf)

        # region attributes
        self.water = False
        self.beach = False
        self.road = False
        self.map_edge = False

    def set_mask(self, mask_surf):
        if mask_surf is None:
            self.mask_surf = None
            self.mask = None
            self.centroid = None
            return

        self.mask_surf = mask_surf.convert()
        self.mask = pygame.mask.from_threshold(self.mask_surf, (255,255,255), (1,1,1))
        self.centroid = (self.mask.centroid()[0] + self.rect.left,
                         self.mask.centroid()[1] + self.rect.top)


    def add_neighbor(self, neighbor):
        if not isinstance(neighbor, type(self)):
            return

        if neighbor != self:
            self.neighbors.add(neighbor)
            neighbor.neighbors.add(self)

    def collide_point(self, pos):
        """Test if a given position is in this region. Pixel-perfect collision detection."""

        # first compare with our bounding rectangle
        if not self.rect.collidepoint(pos):
            return False

        # check the mask
        return 0 != self.mask.get_at((pos[0] - self.rect.left,
                                      pos[1] - self.rect.top))

    def to_json(self):
        """Serialise myself as a JSON string"""
        retval = {
            "__type__": "Region",
            "identifier": self.identifier,
            "rect": (self.rect.topleft, self.rect.size),

            "water": self.water,
            "beach": self.beach,
            "road": self.road,
            "map_edge": self.map_edge
            }

        retval["neighbors"] = [ x.identifier for x in self.neighbors ]

        return retval


if __name__ == "__main__":
    import game
    try:
        game.main()
    finally:
        pygame.quit()
    print("Program ending...")
