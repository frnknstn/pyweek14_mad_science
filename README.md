# Mad Science #

Incomplete entry for pyweek 14.

Possibly my least-complete entry.

The notable component is code/prepare_map.py, which given a .png with multiple regions of unique colours, will divide the map into regions and calculate which regions are adjacent to each other.

# History #

My first pyweek attempt. I was working on it after I got home from the job, having a great time. Then:

* I hit another pygame bug
* I reported the problem to the pygame mailing list along with suggested fix. I don't think they ever did anything about it.
* I spent a day implementing a tool feature. I then discovered that exact functionality was already in the lib. I lost motivation shortly after that.

